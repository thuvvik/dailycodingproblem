#!/bin/sh

# creates a solution containing:
# a console project
# a xunit test project
# with the name given:

# example skeleton.sh 3 CocoLuna

PROJECT_NAME=$1

dotnet new sln --name $PROJECT_NAME --output ./$PROJECT_NAME
dotnet new xunit --name Tests --output ./$PROJECT_NAME/Tests --no-restore
dotnet new console --name Cli --output ./$PROJECT_NAME/Cli --no-restore
dotnet add ./$PROJECT_NAME/Tests/Tests.csproj reference ./$PROJECT_NAME/Cli/Cli.csproj
dotnet sln ./$PROJECT_NAME/$PROJECT_NAME.sln add ./$PROJECT_NAME/Tests/Tests.csproj
dotnet sln ./$PROJECT_NAME/$PROJECT_NAME.sln add ./$PROJECT_NAME/Cli/Cli.csproj
dotnet restore ./$PROJECT_NAME/$PROJECT_NAME.sln
cat >./$PROJECT_NAME/.gitignore <<EOL
*.swp
*.*~
project.lock.json
.DS_Store
*.pyc

# Visual Studio Code
.vscode

# User-specific files
*.suo
*.user
*.userosscache
*.sln.docstates

# Build results
[Dd]ebug/
[Dd]ebugPublic/
[Rr]elease/
[Rr]eleases/
x64/
x86/
build/
bld/
[Bb]in/
[Oo]bj/
msbuild.log
msbuild.err
msbuild.wrn

# Visual Studio 2015
.vs/
EOL
git add ./$PROJECT_NAME/.gitignore
git commit -m "New project ${PROJECT_NAME}, C# git ignore file committed"
echo '' > ./$PROJECT_NAME/Readme.md