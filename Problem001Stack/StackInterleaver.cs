using System;
//using System.Collections; // Queue
using System.Collections.Generic; // Queue<T>
using System.Linq; // Queue.Reverse

namespace Problem001Stack
{
    class StackInterleaver<T>
    {
        private Queue<T> _helper;

        public StackInterleaver()
        {
            this._helper = new Queue<T>();
        }

        public Stack<T> act(Stack<T> stack)
        {
            // bottom of the stack is already well placed, so we ignore it 
            int itemCount = (stack.Count-1); 
            while(itemCount > 0)
            {
                stack = this.reverseOrder(stack, itemCount);
                itemCount--;
            }

            // reverse order
            return this.reverseOrder(stack);
        }
         /// <summary>
        /// reverse a Stack using a Queue as helper
        /// </summary>
        private Stack<T> reverseOrder(Stack<T> toBeReversed, int itemCount = -1)
        {
            if (itemCount == -1) {
                itemCount = toBeReversed.Count;
            }

            for(int times=0; times < itemCount; times++)
            {
                this._helper.Enqueue(toBeReversed.Pop());
            }
            for(int times=0; times < itemCount; times++)
            {
                toBeReversed.Push(this._helper.Dequeue());
            }

            return toBeReversed;
        }
    }
}