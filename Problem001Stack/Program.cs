﻿using System;
//using System.Collections; // Queue
using System.Collections.Generic; // Queue<T>
using System.Linq; // Queue.Reverse

namespace Problem001Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> example = new Stack<int>(new[]{ 1, 2, 3, 4, 5 });
            StackInterleaver<int> sil = new StackInterleaver<int>();

            Stack<int> results = sil.act(example);

            Console.WriteLine("Results:");
            foreach(int item in results)
            {
                Console.WriteLine(item);
            }
        }
    }
}
