
Given a stack of N elements, interleave the first half of the stack with the second half reversed using only one other queue. 

For example, if the stack is [1, 2, 3, 4, 5], it should become [1, 5, 2, 4, 3].

20171207: solution found on the web http://www.geeksforgeeks.org/interleave-first-half-queue-second-half/