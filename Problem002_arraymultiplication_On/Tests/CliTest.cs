using System;
using System.Collections;
using Xunit;

using Problem2Cli;

namespace Problem2Tests
{
    public class CliTest
    {
        [Fact]
        public void TestAct()
        {
            ArrayMultiplier am = new ArrayMultiplier();
            int[] actualResults = am.act(new int[]{ 1, 2, 3, 4, 5 });

            Assert.Equal(5, actualResults.Length);
            int[] expectedResults = new int[]{ 120, 60, 40, 30, 24 };
            for(int indexControl =0; indexControl<actualResults.Length; indexControl++)
            {
                Assert.Equal(expectedResults[indexControl], actualResults[indexControl]);
            }
        }
    }
}
