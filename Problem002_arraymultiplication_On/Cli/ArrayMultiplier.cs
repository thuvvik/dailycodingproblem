using System;

namespace Problem2Cli
{
    public class ArrayMultiplier
    {
        public int[] act(int[] arrayParameter)
        {
            int[] resultsForward = new int[arrayParameter.Length];
            int[] finalResult = new int[arrayParameter.Length];

            for(int indexForward = 0; indexForward < arrayParameter.Length; indexForward++) 
            {
                resultsForward[indexForward] = 1;
                if (indexForward > 0) {
                    resultsForward[indexForward] = arrayParameter[(indexForward-1)] * resultsForward[(indexForward-1)];
                }
            }

            int backwardMultiplication = 1;
            for(int indexBackward = (arrayParameter.Length-1); indexBackward >= 0; indexBackward--)
            {
                finalResult[indexBackward] = resultsForward[indexBackward];
                if (indexBackward != (arrayParameter.Length-1)) {
                    backwardMultiplication = arrayParameter[(indexBackward+1)] * backwardMultiplication;
                    finalResult[indexBackward] = finalResult[indexBackward] * backwardMultiplication;
                }
            }

            return finalResult;
        }
    }
}