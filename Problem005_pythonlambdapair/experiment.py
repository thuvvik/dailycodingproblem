def cons(a, b):
    return lambda f: f(a, b)

def car(pair):
    return pair(lambda a,b: a)

def cdr(pair):
    return pair(lambda a,b: b)

print(car(cons(1,2)))
print(cdr(cons(1,2)))
