﻿using System;

namespace Cli
{
    class Program
    {
        static void Main(string[] args)
        {

            // SortedSet => https://stackoverflow.com/questions/3262947/is-there-a-built-in-binary-search-tree-in-net-4-0

            BinaryTreeNode<int> root = new BinaryTreeNode<int>(1);
            root.left = new BinaryTreeNode<int>(2);
            root.right = new BinaryTreeNode<int>(3);
            root.left.left = new BinaryTreeNode<int>(4);
            root.left.right = new BinaryTreeNode<int>(5);
            root.right.left = new BinaryTreeNode<int>(6);
            root.right.right = new BinaryTreeNode<int>(7);
            //Program.PrintBinaryTreeNode(root, 0, "");

            TreeSerializer<int> ts = new TreeSerializer<int>();
            //Console.WriteLine(ts.serialize(root));

            String  serialized = "{1,[l{2,[l{4,[]},r{5,[]}]},r{3,[l{6,[]},r{7,[]}]}]}" ;
            BinaryTreeNode<int> root2 = ts.unserialize(serialized);

            Program.PrintBinaryTreeNode(root2, 0, "");
        }

        static void PrintBinaryTreeNode(BinaryTreeNode<int> node, int level = 0,String additional = "")
        {
            String decalage = "" + new String('-', level) + additional ;
            Console.WriteLine( decalage + node.data.ToString());
            if (node.left != null)
            {
                Program.PrintBinaryTreeNode(node.left, level +1, " left: ");
            }
            if (node.right!= null)
            {
                Program.PrintBinaryTreeNode(node.right, level +1, "right: ");
            }
        }
    }
}
