using System;
using System.Runtime.Serialization;

namespace Cli
{
    class BinaryTreeNode<T> where T : IConvertible
    {
        public BinaryTreeNode() {} // necessary for serialization ?
        public BinaryTreeNode(T data)
        {
            this.data = data;
        }

        public T data { get; set; }
        public BinaryTreeNode<T> left { get; set; }
        public BinaryTreeNode<T> right { get; set; }
    }

}