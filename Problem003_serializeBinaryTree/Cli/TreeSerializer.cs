using System;
using System.Text.RegularExpressions;

namespace Cli
{
    class TreeSerializer<T> where T : IConvertible
    {
        public String serialize(BinaryTreeNode<T> node)
        {
            String serialized = "";

            serialized += "{";
            serialized += node.data.ToString();
            serialized += ",";
            serialized += "[";
            bool leftPresent = (node.left != null);
            if (leftPresent) 
            {
                serialized += "l" + this.serialize(node.left);
            }
            if(node.right != null) 
            {
                if (leftPresent)
                {
                    serialized += ",";
                }
                serialized += "r" + this.serialize(node.right);
            }
            serialized += "]";
            serialized += "}";
            return serialized;
        }

        public BinaryTreeNode<T> unserialize(String toRead)
        {
            Console.WriteLine("passage: " + toRead + "\n");
            BinaryTreeNode<T> node = null;
            if ((toRead[0] == '{') && (toRead[(toRead.Length-1)] == '}'))
            {
                toRead = toRead.Substring(1, toRead.Length - 2); // remove { and }
                string[] split = toRead.Split(new char[] { ',' }, 2);
                node = new BinaryTreeNode<T>( (T) Convert.ChangeType(split[0], typeof(T)));
                if (split[1].Length > 2) // != "[]"
                {
                    toRead = split[1].Substring(1, split[1].Length - 2); // remove [ and ]
                    
                    Regex rx = new Regex(@"
                        (l
                            (?<left>
                                \{
                                    (?:
                                        [^{}]*
                                        | (?<open>\{)
                                        | (?<-open>\})
                                    )+
                                    (?(open)(?!))
                                \}
                            )
                        )?
                        (?:,?)
                        (r 
                            (?<right>
                                \{
                                    (?:
                                        [^{}]*
                                        | (?<open>\{)
                                        | (?<-open>\})
                                    )+
                                    (?(open)(?!))
                                \}
                            )
                        )?
                    ", RegexOptions.IgnorePatternWhitespace);
                    Match match = rx.Match(toRead);
                    if(match.Success) {
                        var left = match.Groups["left"];
                        var right = match.Groups["right"];
                        if (left.Success)
                        {
                            node.left = this.unserialize(left.Value);
                        }
                        if (right.Success)
                        {
                            node.right = this.unserialize(right.Value);
                        }
                    }                   
                } 
            }
            return node;            
        }
    }
}