using System;
using Xunit;

using Cli;

namespace Tests
{
    public class FinderTest
    {
        [Theory]
        [InlineData(2, new int[]{3, 4, -1, 1})]
        [InlineData(3, new int[]{1, 2, 0})]
        public void TestFindMissingViaLinq(int expectedValue, int[] arrayToSearch)
        {   
            // arrange 
            Finder f = new Finder();

            // Act
            int actualValue = f.findMissingViaLinq(arrayToSearch);

            // Assert
            Assert.Equal(actualValue, expectedValue);
        }
    }
}
