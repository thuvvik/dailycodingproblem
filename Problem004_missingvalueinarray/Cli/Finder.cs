using System;
using System.Linq;

namespace Cli
{
    public class Finder
    {
        public int findMissingViaLinq(int[] arrayToSearch)
        {
            return Enumerable.Range(1, Int32.MaxValue).Except(arrayToSearch).First();
        }
    }
}
